﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;



public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    //used for analytics
    private float levelTimer;
    private int levelRedo;
    private int badMatchesPerLevel;
    private bool[,] tilesUncovered;
    private int rows;
    private int cols;
    private int mouseClicks;

    private void Start()
    {
        LoadLevel( CurrentLevelNumber );
    }



    private void Update()
    {
        levelTimer += Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            mouseClicks++;
        }
    }

   


    //gets called to load a new level and calls to set up the camera
    private void LoadLevel( int levelNumber )
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );


        //sets up tiles uncovered;
        tilesUncovered = new bool[level.Rows, level.Columns];
        rows = level.Rows;
        cols = level.Columns;

        //populates the tiles into the level and gives them the proper symbol
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );

        

        //level reached
        AnalyticsEvent.Custom("Level Reached", new Dictionary<string, object> { { "Level " , CurrentLevelNumber +1} });
        // AnalyticsEvent.Custom(string customEventName, IDictionary<string, object> eventData);
    }

    //returns the correct symbols for a given level
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //adds symbols to the Symbols list
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //checks if the right number of symbols were created for the number of cards.
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        //decides how many duplicates there should be of symbols and where to place them
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //returns the right offset to place the material on each card properly
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    //places the camera to view the current level from the right position
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //reveals tiles selected to show the symbol and checks if they have the same symbol
    public void TileSelected( Tile tile )
    {

     

        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );
            }
            else
            {

                //updates if a tile has been uncovered before
                if (!tilesUncovered[(int)(m_tileOne.gameObject.transform.position.y / (1 + TileSpacing)), (int)(m_tileOne.gameObject.transform.position.x / (1 + TileSpacing))])
                {
                    tilesUncovered[(int)(m_tileOne.gameObject.transform.position.y / (1 + TileSpacing)), (int)(m_tileOne.gameObject.transform.position.x / (1 + TileSpacing))] = true;
                }

                if (!tilesUncovered[(int)(m_tileTwo.gameObject.transform.position.y / (1 + TileSpacing)), (int)(m_tileTwo.gameObject.transform.position.x / (1 + TileSpacing))])
                {
                    tilesUncovered[(int)(m_tileTwo.gameObject.transform.position.y / (1 + TileSpacing)), (int)(m_tileTwo.gameObject.transform.position.x / (1 + TileSpacing))] = true;
                }


                //tracks bad matches
                badMatchesPerLevel++;
              
                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }
    }

    //is called when the level is complete and starts a new level.
    private void LevelComplete()
    {
     
        //time to finish the level
        AnalyticsEvent.Custom("Time Per Level", new Dictionary<string, object> { { "Level " + (CurrentLevelNumber+1), levelTimer } });
        levelTimer = 0;
        // AnalyticsEvent.Custom(string customEventName, IDictionary<string, object> eventData);

        //bad matches tracker
        AnalyticsEvent.Custom("Bad Matches", new Dictionary<string, object> { { "Level " + (CurrentLevelNumber + 1), badMatchesPerLevel } });
        // AnalyticsEvent.Custom(string customEventName, IDictionary<string, object> eventData);
        badMatchesPerLevel = 0;


        int luckyCounter = 0;

        for (int rowIndex = 0; rowIndex <rows; ++rowIndex)
        {
            for (int colIndex = 0; colIndex < cols; ++colIndex)
            {
               if(!tilesUncovered[rowIndex, colIndex])
                {
                    luckyCounter++;
                }
            }
        }

        //level reached
        AnalyticsEvent.Custom("Lucky Cards Matched", new Dictionary<string, object> { { "Level "+ (CurrentLevelNumber + 1), luckyCounter } });
        // AnalyticsEvent.Custom(string customEventName, IDictionary<string, object> eventData);


        //level reached
        AnalyticsEvent.Custom("Clicks Per Level", new Dictionary<string, object> { { "Level " + (CurrentLevelNumber + 1),mouseClicks } });
        // AnalyticsEvent.Custom(string customEventName, IDictionary<string, object> eventData);
        mouseClicks = 0;


        ++CurrentLevelNumber;
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );
        }
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    // hides or destroys cards after 2 have been chosen depending on if the 2 cards match
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
            LevelComplete();
        }
    }
}
